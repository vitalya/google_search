# Используем официальный образ Python
FROM python:3.11

# Устанавливаем зависимости
COPY requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir -r /app/requirements.txt

# Копируем исходный код приложения в образ
COPY . /app

# Устанавливаем рабочую директорию
WORKDIR /app

# Команда для запуска скрипта
CMD ["python", "bot.py"]
