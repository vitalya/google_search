import re

import googlemaps
import dotenv
import os

import requests

dotenv.load_dotenv()



def find_emails(url):
    if url is None:
        return None
    if not 'http' in url:
        url = 'https://' + url
    try:
        response = requests.get(url)
    except:
        return None
    if response.status_code == 200:
        emails = re.findall('([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)', response.text)

        return set(emails)
    else:
        return None


def get_place_details(place_id):
    gmaps = googlemaps.Client(key=os.getenv('GOOGLE_MAPS_API_KEY'))

    place = gmaps.place(place_id)

    place_details = {"name": '', 'address': '', 'website': '', 'phone': '', 'emails': ''}

    try:
        place = place['result']
    except:
        return None

    if 'formatted_phone_number' in place.keys():
        place_details['phone'] = place['formatted_phone_number']

    if 'formatted_address' in place.keys():
        place_details['address'] = place['formatted_address']

    if 'name' in place.keys():
        place_details['name'] = place['name']

    if 'website' in place.keys():
        place_details['website'] = place['website']
        place_details['emails'] = find_emails(place['website'])

    return place_details


def execute(query):
    gmaps = googlemaps.Client(key=os.getenv('GOOGLE_MAPS_API_KEY'), queries_per_second=1000, queries_per_minute=10000)

    places = gmaps.places(query)

    place_details = []
    while True:
        for place in places['results']:
            place_data = get_place_details(place['place_id'])
            if place_data:
                place_details.append(place_data)
        if 'next_page_token' in places:
            next_page_token = places['next_page_token']
            places = gmaps.places(query, page_token=next_page_token)
        else:
            break
    return place_details


